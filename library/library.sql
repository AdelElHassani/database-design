DROP DATABASE IF EXISTS library;
CREATE DATABASE library;

USE library;

SET sql_mode = "STRICT_ALL_TABLES";


CREATE TABLE streetRef (
  name VARCHAR(50) NOT NULL
    CONSTRAINT CC_STREETCHECK CHECK (LENGTH(name) <= 50),
  PRIMARY KEY (name)
);

CREATE TABLE cityRef (
  name VARCHAR(50) NOT NULL
    CONSTRAINT CC_CITYCHECK CHECK (LENGTH(name) <= 50),
  PRIMARY KEY (name)
);

CREATE TABLE regionRef (
  name VARCHAR(50) NOT NULL
    CONSTRAINT CC_REGIONCHECK CHECK (LENGTH(name) <= 50),
  PRIMARY KEY (name)
);

CREATE TABLE postalRef (
  postalCode INT NOT NULL
    CONSTRAINT CC_CODECHECK CHECK (postalCode > 0),
  PRIMARY KEY (postalCode)
);

CREATE TABLE genderRef (
  genderKey CHAR(1) NOT NULL
    CONSTRAINT CC_GENDERCHECK CHECK ((genderKey = 'F') OR (genderKey = 'M') OR (genderKey = 'N')),
  PRIMARY KEY (genderKey)
);
INSERT INTO genderRef (genderKey) VALUES ('F'), ('M'), ('N');

CREATE TABLE contactInfo (
  infoId INT NOT NULL AUTO_INCREMENT,
  phone VARCHAR(10) NOT NULL UNIQUE
    CONSTRAINT CC_ContactPhone CHECK (REGEXP_LIKE(phone, '^[0-9]{10}$')),
  email VARCHAR(255) NOT NULL UNIQUE
    CONSTRAINT CC_ContactEmail CHECK ((REGEXP_LIKE(email, '^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$'))
    AND (LENGTH(email) <= 255)),
  PRIMARY KEY (infoId)
);

CREATE TABLE personInfo (
  personId INT NOT NULL AUTO_INCREMENT,
  firstName VARCHAR(50) NOT NULL
    CONSTRAINT CC_PersonFName CHECK (REGEXP_LIKE(firstName, '^[a-zA-Z\s]{,50}$')),
  lastName VARCHAR(50) NOT NULL
    CONSTRAINT CC_PersonLName CHECK (REGEXP_LIKE(lastName, '^[a-zA-Z\s]{,50}$')),
  genderKey CHAR(1) NOT NULL
    CONSTRAINT CC_PersonGender CHECK ((genderKey = 'F') or (genderKey = 'M')),
  bDate DATE NOT NULL DEFAULT (CURDATE()),
  PRIMARY KEY (personId),
  FOREIGN KEY (genderKey) REFERENCES genderRef(genderKey)
);

CREATE TABLE writer (
  writerId INT NOT NULL AUTO_INCREMENT,
  personId INT NOT NULL,
  infoId INT NOT NULL,
  PRIMARY KEY (writerId),
  FOREIGN KEY (personId) REFERENCES personInfo(personId),
  FOREIGN KEY (infoId) REFERENCES contactInfo(infoId)
);

CREATE TABLE editor (
  editorId INT NOT NULL AUTO_INCREMENT,
  editorName VARCHAR(50) NOT NULL
    CONSTRAINT CC_EditorName CHECK (LENGTH(editorName) <= 50),
  PRIMARY KEY (editorId)
);

CREATE TABLE warehouse (
  warehouseId INT NOT NULL AUTO_INCREMENT,
  warehouseName VARCHAR(50) NOT NULL
    CONSTRAINT CC_WarehouseName CHECK (LENGTH(warehouseName) <= 50),
  PRIMARY KEY (warehouseId)
);

CREATE TABLE address (
  addressId INT NOT NULL AUTO_INCREMENT,
  streetNbr INT NOT NULL
    CONSTRAINT CC_StreenNbr CHECK (streetNbr > 0),
  street VARCHAR(50) NOT NULL,
  postalCode INT NOT NULL,
  city VARCHAR(50) NOT NULL,
  region VARCHAR(50) NOT NULL,
  PRIMARY KEY (addressId),
  FOREIGN KEY (street) REFERENCES streetRef(name),
  FOREIGN KEY (postalCode) REFERENCES postalRef(postalCode),
  FOREIGN KEY (city) REFERENCES cityRef(name),
  FOREIGN KEY (region) REFERENCES regionRef(name),
  CONSTRAINT CU_address UNIQUE (streetNbr, street, postalCode, city, region)
);

CREATE TABLE writerAddress (
  writerId INT NOT NULL,
  addressId INT NOT NULL,
  PRIMARY KEY (writerId, addressId),
  FOREIGN KEY (writerId) REFERENCES writer(writerId),
  FOREIGN KEY (addressId) REFERENCES address(addressId)
);

CREATE TABLE editorAddress (
  editorId INT NOT NULL,
  addressId INT NOT NULL,
  PRIMARY KEY (editorId, addressId),
  FOREIGN KEY (editorId) REFERENCES editor(editorId),
  FOREIGN KEY (addressId) REFERENCES address(addressId)
);

CREATE TABLE editorContact (
  editorId INT NOT NULL,
  personId INT NOT NULL,
  infoId INT NOT NULL,
  PRIMARY KEY (editorId, infoId),
  FOREIGN KEY (editorId) REFERENCES editor(editorId),
  FOREIGN KEY (personId) REFERENCES personInfo(personId),
  FOREIGN KEY (infoId) REFERENCES contactInfo(infoId)
);

CREATE TABLE warehouseAddress (
  warehouseId INT NOT NULL,
  addressId INT NOT NULL,
  PRIMARY KEY (warehouseId, addressId),
  FOREIGN KEY (warehouseId) REFERENCES warehouse(warehouseId),
  FOREIGN KEY (addressId) REFERENCES address(addressId)
);

CREATE TABLE warehouseContact (
  warehouseId INT NOT NULL,
  personId INT NOT NULL,
  infoId INT NOT NULL,
  PRIMARY KEY (warehouseId, personId),
  FOREIGN KEY (warehouseId) REFERENCES warehouse(warehouseId),
  FOREIGN KEY (personId) REFERENCES personInfo(personId),
  FOREIGN KEY (infoId) REFERENCES contactInfo(infoId)
);

CREATE TABLE book (
  title VARCHAR(50) NOT NULL
    CONSTRAINT CC_PRODCHECK CHECK (LENGTH(title) <= 50),
  PRIMARY KEY (title)
);

CREATE TABLE bookEdition (
  title VARCHAR(50) NOT NULL,
  edition INT NOT NULL,
  isbn VARCHAR(13) NOT NULL UNIQUE,
  PRIMARY KEY (title, edition),
  FOREIGN KEY (title) REFERENCES book(title),
  CONSTRAINT CU_BookRef UNIQUE (isbn, edition)
);

CREATE TABLE bookEditor (
  title VARCHAR(50) NOT NULL,
  editorId INT NOT NULL,
  PRIMARY KEY (title, editorid),
  FOREIGN KEY (editorId) REFERENCES editor(editorId),
  FOREIGN KEY (title) REFERENCES book(title)
);

CREATE TABLE bookWriter (
  title VARCHAR(50) NOT NULL,
  writerId INT NOT NULL,
  PRIMARY KEY (title, writerId),
  FOREIGN KEY (writerId) REFERENCES writer(writerId),
  FOREIGN KEY (title) REFERENCES book(title)
);

CREATE TABLE bookStock (
  stockId INT NOT NULL AUTO_INCREMENT,
  isbn VARCHAR(13) NOT NULL,
  insertDate DATE NOT NULL DEFAULT (CURDATE()),
  price DECIMAL(10,2) NOT NULL
    CONSTRAINT CC_PriceCheck CHECK (price >= 0),
  quantity INT NOT NULL
    CONSTRAINT CC_Quantity CHECK (quantity >= 0),
  PRIMARY KEY (stockId),
  FOREIGN KEY (isbn) REFERENCES bookEdition(isbn)
);